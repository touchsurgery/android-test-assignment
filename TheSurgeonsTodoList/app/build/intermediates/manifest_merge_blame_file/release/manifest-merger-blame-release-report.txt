1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.touchsurgery.thesurgeonstodolist"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="27"
8-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="31" />
9-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml
10
11    <application
11-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:3:5-20:19
12        android:allowBackup="true"
12-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:4:13-39
13        android:appComponentFactory="android.support.v4.app.CoreComponentFactory"
13-->[com.android.support:support-compat:28.0.0] /Users/orlang3/.gradle/caches/transforms-3/360d1ba55a28185f41fa4b803e4a2061/transformed/support-compat-28.0.0/AndroidManifest.xml:22:18-91
14        android:extractNativeLibs="false"
15        android:icon="@mipmap/ic_launcher"
15-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:5:13-47
16        android:label="@string/app_name"
16-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:6:13-45
17        android:roundIcon="@mipmap/ic_launcher_round"
17-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:7:13-58
18        android:supportsRtl="true"
18-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:8:13-39
19        android:theme="@style/AppTheme" >
19-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:9:13-44
20        <activity
20-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:10:9-18:20
21            android:name="com.touchsurgery.thesurgeonstodolist.activities.MainActivity"
21-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:11:17-56
22            android:exported="true" >
22-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:12:17-40
23            <intent-filter>
23-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:13:13-17:29
24                <action android:name="android.intent.action.MAIN" />
24-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:14:17-69
24-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:14:25-66
25
26                <category android:name="android.intent.category.LAUNCHER" />
26-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:16:17-77
26-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:16:27-74
27            </intent-filter>
28        </activity>
29        <activity android:name="com.touchsurgery.thesurgeonstodolist.activities.SettingsActivity" />
29-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:19:9-65
29-->/Users/orlang3/android-test-assignment/TheSurgeonsTodoList/app/src/main/AndroidManifest.xml:19:19-62
30    </application>
31
32</manifest>
